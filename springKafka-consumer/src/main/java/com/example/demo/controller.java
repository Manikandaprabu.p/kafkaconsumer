package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200/")
public class controller {
    
	@Autowired
	private KafkaConsumer kafkaConsumer;
	
	
	@GetMapping("/hii/{email}")
	public String getMail(@PathVariable("email") String data) {
		return kafkaConsumer.meth(data);
	}
	
}
