package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {
 
	String s="";
	String toEmail="";
	String subject="PortStatus";
	
	
	@Autowired
	private JavaMailSender mailSender;
	
	
	@KafkaListener(topics="hey", groupId="email-consumer")
	public void consume(String message) {
		 s = message;
		 SimpleMailMessage mail = new SimpleMailMessage();
		 mail.setFrom("manikandaprabu635@gmail.com");
		 mail.setTo(toEmail);
		 mail.setText(message);
		 mail.setSubject(subject);
		 
		 mailSender.send(mail);
		 
//		System.out.println("Massage Consumed:" + message);
		 System.out.println("Successfull");
	}
	
	public String meth(String data1) {
		toEmail = data1;
		return "success";
	}
	
}
